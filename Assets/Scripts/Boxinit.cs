﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boxinit : MonoBehaviour
{
    public GameObject boxObjPrefab;
    public GameObject boxesObj;

    // Startよりも早く呼ばれるイベント巻数
    private void Awake()
    {

        // MasterオブジェクトをFindしてGameObject変数に代入
        GameObject masterObj = GameObject.Find("Master");

        //for文で横8×縦5にBoxのPrefabをインスタンス化しています
        for (int x= 0; x < 8; x++)
        {
            for(int y= 0;y < 5; y++)
            {
                // PrefabからBoxオブジェクトをインスタンス化                
                GameObject g = Instantiate(boxObjPrefab
                                          , boxesObj.transform);
                g.transform.position =
                    new Vector3((2f + (1f * y))
                                ,0.4f
                                , (-4.2f + (1.2f * x))
                                );
                // Destroyer コンポーネントのmasterObjに
                // Masterオブジェクトを渡す
                g.GetComponent<Destroyer>().masterObj = masterObj;
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
    
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
